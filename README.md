## Capstone 1

In the event of a depression or a recession (2000-2018), which countries perform the best GDP growht by the 5th year?

- [DataMap](https://1drv.ms/b/s!AnlNcBcZ7EwSl3R3PBkKZVx_-cE_?e=lchq63) - links to data from [data.worldbank.org](data.worldbank.org)
- [Tableau](https://public.tableau.com/views/CountriesReactingtoEconomicalCrises/GDPGrowthintheeventofaRecession?:language=en&:display_count=y&publish=yes&:origin=viz_share_link)


## ChemCorp Case Study

Economic data analysis on the impact of the current market prices, to intervene on the operating break-even point, with particular reference to maintenance expenditure.

- [Problem Statement](https://1drv.ms/p/s!AnlNcBcZ7EwSmgtR6RO_Bh0FuV42?e=wodlJL)
- [Tableau](https://public.tableau.com/views/ChemCorpAnalysis_16005452045130/ChemCorpAnalysis?:language=en&:display_count=y&publish=yes&:origin=viz_share_link)
- [Dataset](https://1drv.ms/x/s!AnlNcBcZ7EwSmg2rYGv-QhkicLxG?e=45u578) (xlsx)


## WaterCorp Case Study 

Economic Data Analysis of the tight-competition in the Desalinated Water Market.

- [Market Elasticity Analysis ](https://1drv.ms/x/s!AnlNcBcZ7EwSmg61pnbqDV1CXHI1?e=ABE2uU) (xlsx)
- [Revenue, Expenditure and Profit (EBIT) Analysis](https://1drv.ms/x/s!AnlNcBcZ7EwSmhAOEZut8dnRnDBm?e=DiUKHd) (xlsx)


## Descriptive and Inferential Stats Case Study

Analysis of the reliability of the desalination plants. Modeling of core signal treshholds to prevent desalination pump failures.

- [Analysis](https://1drv.ms/p/s!AnlNcBcZ7EwSmgF1iCUc3t56Cgfr?e=CMU7TE) (pptx, with charts created with xlsx)
